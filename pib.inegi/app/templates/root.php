<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>

 <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
	<div class='container'>
<p><h3><strong>DOCUMENTACIÓN API INEGI PIB</strong></h3></p>
<p><strong>INTEGRANTES:</strong></p>
<p>-López Ojeda Christina<br />
-Mendiola Sarabio Bernardo<br />
-Pinacho García Gibran Eleacid<br />
-Morales Lorenzo Agustín</p>

<div class='bg-info'>
<p><center class='bg-primary'>MÉTODOS GET</center></p>

<p >&quot;/api/entidades&quot;: Devuelve todas las entidades </p>
<p>&quot;/api/entidades/$id&quot;: Mostrar la entidad con el id específicado</p>
<p>&quot;/api/municipios&quot;: Mostrar todos los municipios</p>
<p>&quot;/api/municipios/$id&quot;: Mostrar el municipio con el id especificado</p>
<p>&quot;/api/indicadores&quot;: Obtener todos los indicadores.</p>
<p>&quot;/api/indicadores/$id&quot;: Obtener indicador con el id especificado</p>
<p>&quot;/api/temas-nivel-1&quot;: Obtener todos los temas de nivel 1</p>
<p>&quot;/api/temas-nivel-1/$id&quot;: Obtener todos el tema nivel 1 con el id especificado</p>
<p>&quot;/api/temas-nivel-2&quot;: Obtener todos los temas de nivel 2</p>
<p>&quot;/api/temas-nivel-2/$id&quot;: Obtener el tema nivel 2 con el id especificado</p>
<p>&quot;/api/temas-nivel-3&quot;: Obtener todos los temas de nivel 3</p>
<p>&quot;/api/temas-nivel-3/$id&quot;: Obtener todos los temas de nivel 3 con el id especificado</p>
<p>&quot;/api/temas/indicadores-monto&quot;: Obtener todos los registros de la tabla indicadores-monto</p>
<p>&quot;/api/indicadores-monto/clave/$cl&quot; obtener el registro de indicadores-monto con la clave especificada</p>
<p>&quot;/api/indicadores-monto/entidades/$entidad&quot; obtener el registro indicadores-monto de la entidad especificada</p>
<p>&quot;/api/indicadores-monto/municipios/$municipio&quot; obtener el registro de indicadores-monto del municipio especificado</p>
<p>&quot;/api/indicadores-monto/temas_nivel1/$n1&quot; obtener el registro de indicadores-monto del tema nivel 1 especificado</p>
<p>&quot;/api/indicadores-monto/temas_nivel2/:n2&quot; obtener el registro de indicadores-monto del tema-nivel 2 especificado</p>
<p>&quot;/api/indicadores-monto/annio/$anio&quot; obtener los registros de indicadores-monto del año especificado</p>
<p>&nbsp;</p>
</div>


<div class='bg-info'>
<p><center class='bg-primary'>MÉTODOS POST y PUT</center></p>

<p>&quot;/api/entidades/&quot;: insertar o actualizar una entidad</p>
<p>&quot;/api/municipios/&quot;: insertar o actualizar un  municipio</p>
<p>&quot;/api/indicadores/&quot;: insertar o actualizar un indicador</p>
<p>&quot;/api/indicadores-monto/&quot; insertar o actualizar indicador-monto</p>

<p>&nbsp;</p>
</div>

<div class='bg-info'>
<p><center class='bg-primary'>MÉTODOS DELETE</center></p>


<p>&quot;/api/entidades/$id&quot;: Eliminar la entidad con el id específicado</p>
<p>&quot;/api/municipios/$id&quot;: Eliminar el municipio con el id especificado</p>
<p>&quot;/api/indicadores/$id&quot;: Eliminar indicador con el id especificado</p>
<p>&quot;/api/temas-nivel-1/$id&quot;: Eliminar todos el tema nivel 1 con el id especificado</p>
<p>&quot;/api/temas-nivel-2/$id&quot;: Eliminar el tema nivel 2 con el id especificado</p>
<p>&quot;/api/temas-nivel-3/$id&quot;: Eliminar todos los temas de nivel 3 con el id especificado</p>
<p>&nbsp;</p>
</div>

</div>
</body>
</html>
